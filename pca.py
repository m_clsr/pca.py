# -*- coding: utf-8 -*-

"""
Core module of PCAv2.

© 2017 by :ref:`Pull'n'Pusz <about>`.
"""

from collections import defaultdict, deque, namedtuple
from math import copysign
import re
import sys
from time import time
from threading import Thread
from queue import Queue, Empty

from requests import post
import sqlite3

from config import Config

TARGET = Config.default['TARGET']
MAX_INCR = Config.default['MAX_INCR']
MAX_DECR = Config.default['MAX_DECR']
MISSING_LIMIT = Config.default['MISSING_LIMIT']
HYSTERESIS = Config.default['HYSTERESIS']
MIN_MEAS = Config.default['MIN_MEAS']
OFFSET = Config.default['OFFSET']
HOST = Config.default['HOST']
WINDOW = Config.default['WINDOW']

PATTERN = r'[DU]L\s+((S0)|(N[0-6]))\s+[a-zA-Z0-9]+\s+((-(4[5-9]\s*(\s+[0-5]\s*|$))|((-9[0-5]\s*(\s+[0-5]\s*|$))|(-[5-8][0-9]\s*(\s+[0-5]\s*|$))|(MISSING|missing)\s*)))$'

RE_OBJ = re.compile(PATTERN)

MeasurementTuple = namedtuple('measurement', ['timestamp', 'direction', 'bts_id', 'ms_id', 'signal', 'quality'])
"""Wraper around normal tuple for convinient measurement data access."""


def sanitize(line):
    """
    Check wheter input is correct or not. According to specification delivered.
    It matches RegEx pattern. Input is not correct it returns None, otherwise returns list of parameters.

    :param line: string consisting line read from sys.stdin
    :type line: string
    :return: list of measurements [float timestamp, str. 'DL'/'UL', str. 'bts_id', str. 'ms_id', str. 'signal', str. 'quality'] or None
    :rtype: list or None
    """

    re_match = RE_OBJ.match(line)
    if re_match is None:
        return None
    else:
        measurement = re_match.group().split()
        measurement.insert(0, time())
        # [int timestamp, 'DL'/'UL', 'bts_id', 'ms_id', 'signal', 'quality']
        if measurement[4] in {'missing', 'MISSING'}:
            measurement[4] = -9999
            measurement.insert(5, 5)
        # all data is explicitd
        elif len(measurement) == 6:
            # signal: str '-75' -> int -75
            measurement[4] = int(measurement[4])
            # quality str '3' -> int 3
            measurement[5] = int(measurement[5])
        # quality parameter not specified
        else:
            measurement[4] = int(measurement[4])
            measurement.insert(5, 5)
    return measurement


class Register(object):
    """
    Class representing data structure and its methods, for storing measurement in memory.
    """

    def __init__(self, window=WINDOW):
        """
        :param window: Maximum amount of measurement stored in structure
        :type window: int. from range <1;8>
        """

        self.struct = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: deque((), window))))
        """Deque inside triple nested dict for convinient data storage"""
        self._weights = (1, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125)
        """Tuple of weigths. weight = n/(2**n) """
        self._weights_sum = {key: value for (key, value) in zip(range(1, 9), [1, 1.5, 1.75, 1.875, 1.9375, 1.96875, 1.984375, 1.9921875])}
        """Dict of sumed weights, for calculating average. Stored static in dictionary for faster computing"""
    def insert(self, direction, ms_id, signal, quality, missing_limit=MISSING_LIMIT):
        """
        Put measurement int self.struct data structure.

        :param direction: direction of signal in given measurement
        :type direction: str. UL or DL
        :param ms_id: mobile station id in given measurement
        :type ms_id: str, id of mobile station
        :param signal: strength of signal in given measurement
        :type signal: int. from range <-75;-45>
        :param quality: value representing quality of signal
        :type quality: int. from range <0;5>
        :param missing_limit: how many missing signals can occur, before value -95 will be assigned to signal
        :type missing_limit: int
        """

        if not isinstance(self.struct[direction][ms_id]['missing'], int):
            self.struct[direction][ms_id]['missing'] = 0
        if signal == -9999:
            if self.struct[direction][ms_id]['missing'] >= missing_limit:
                self.struct[direction][ms_id]['signals'].appendleft(-95)
            else:
                if self.struct[direction][ms_id]['signals']:
                    first_elem = self.struct[direction][ms_id]['signals'][0]
                    self.struct[direction][ms_id]['signals'].appendleft(first_elem)
                else:
                    self.struct[direction][ms_id]['signals'].appendleft(-95)
                self.struct[direction][ms_id]['missing'] += 1
        else:
            self.struct[direction][ms_id]['missing'] = 0
            self.struct[direction][ms_id]['signals'].appendleft(signal)
        self.struct[direction][ms_id]['qualities'].appendleft(quality)

    def enough_measurements(self, direction, ms_id, min_measurements=MIN_MEAS):
        """
        Check if there is enough measurements of signals to start generating output.

        :param direction: direction of signal in given measurement
        :type direction: str. 'UL' or 'DL'
        :param ms_id: mobile station id in given measurement
        :type ms_id: str, id of mobile station
        :param min_measurements: minimum amounts of signals before we can start generating output for given direction-S0-ms_id combination
        :type min_measurements: int. from range <4;8>
        :return: True of False
        :rtype: bool
        """

        return len(self.struct[direction][ms_id]['signals']) >= min_measurements

    def get_avg(self, direction, ms_id, param='signals'):
        """
        Counts an weighted average. Weights decreasing (1/2**n) in exponential orded.
        Weights stored static in self._weights tuple for faster avg calculation.

        :param direction: direction of signal in given measurement
        :type direction: str. 'UL' or 'DL'
        :param ms_id: mobile station id in given measurement
        :type ms_id: str, id of mobile station
        :param param: name of parameter we calculate average for
        :type param: str. 'signals' or 'qualities'
        :return: weighted average
        :rtype: float
        """

        avg = 0
        if not self.struct[direction][ms_id][param]:
            return avg
        else:
            avg = sum((x * w for x, w in zip(self.struct[direction][ms_id][param], self._weights))) / self._weights_sum[len(self.struct[direction][ms_id][param])]
        return avg


class SignalCorrection(object):
    """
    Proposition of signal change.
    """

    def __init__(self, direction, ms_id, signal_avg=None, quality_avg=None):
        self.value = None
        """Value of signal correction"""
        self.ms_id = ms_id
        self.direction = direction
        """Direction of signal"""
        self.signal_avg = signal_avg
        """Average signal"""
        self.quality_avg = quality_avg
        """Average quality"""
    def get_signal_target_difference(self, target=TARGET, max_incr=MAX_INCR, max_decr=MAX_DECR):
        """
        Calculates difference between signal average and desired target value. Updates the self.value
        accordingly to the rules.

        :param target: desired value of signal
        :type target: int or float
        :param max_incr: maximum possible change of signal
        :type max_incr: int or float
        :param max_decr: maximum possible decrease of signal
        :type max_decr: int or float
        """
        delta = target - self.signal_avg
        if delta >= max_incr:
            self.value = max_incr
        elif delta <= max_decr:
            self.value = max_decr
        else:
            self.value = delta

    def apply_hysteresis_filter(self, target=TARGET, hyst=HYSTERESIS):
        """
        Updates signal correction value(self.value) accordingly to hysteresis rules.

        :param target: desired value of signal
        :type target: int or float
        :param hyst: histeresis of target
        :type hyst: int or float
        """
        if -1 < self.value < 1:
            self.value = 0
        else:
            if target + hyst > self.signal_avg > target - hyst:
                self.value = copysign(1, self.value)

    def apply_quality_filter(self):
        """
        Updates signal correction value(self.value) accordingly to signal quality rules.
        """
        if self.quality_avg < 2:
            pass
        elif self.quality_avg >= 4:
            if self.value >= 2:
                pass
            else:
                self.value = 2
        else:
            if self.value > 0:
                pass
            else:
                self.value = 0

    def get_output_format(self):
        """
        Return string with signal value correction proposition, formated in desired way.

        :returns: Formatted string
        :rtype: str
        """
        if self.value <= -1:
            out = "{}  S0  {}  {}  {}".format(self.direction, self.ms_id, "DEC", round(abs(self.value)))
        elif -1 < self.value < 1:
            out = "{}  S0  {}  {}".format(self.direction, self.ms_id, "NCH")
        else:
            out = "{}  S0  {}  {}  {}".format(self.direction, self.ms_id, "INC", round(abs(self.value)))
        return out


class DBHTTPsender(Thread):
    """
    Class definig new thread for putting measurements into database via HTTP-POST-json.

    :param batch_size: batch of measurement size, send to DB
    :type batch_size: int
    :param thread_queue: thread safe queue data structure
    :type thread_queue: queue.Queue
    :param thread_id: ID of thread
    :type thread_id: int
    """

    def __init__(self, thread_queue, thread_id, batch_size=1700):

        super(DBHTTPsender, self).__init__()
        self.thread_id = thread_id
        self.thread_queue = thread_queue
        self._active = True
        self.batch_size = batch_size

    def run(self):
        """
        Thread workflow.
        """
        while self._active or not self.thread_queue.empty():
            payload = {}
            try:
                for key in range(self.batch_size):
                    payload[key] = self.thread_queue.get(block=True, timeout=0.5)
            except Empty:
                post(HOST, json=payload)
            else:
                post(HOST, json=payload)

    def stop(self):
        """
        Method for stopping the thread from working.
        Thread will be stopped only if self.thread_queue is empty
        """
        self._active = False


class DBLocalsender(Thread):
    """
    Class definig new thread for putting measurements into local sqlite3 database.

    :param batch_size: batch of measurements size, send to DB
    :type batch_size: int
    :param thread_queue: thread safe queue data structure
    :type thread_queue: queue.Queue
    :param thread_id: ID of thread
    :type thread_id: int
    """

    def __init__(self, thread_queue, thread_id, batch_size=1000000):

        super(DBLocalsender, self).__init__()
        self.thread_id = thread_id
        self.thread_queue = thread_queue
        self._active = True
        self.batch_size = batch_size

    def run(self):
        """
        Thread worker.
        """
        conn = sqlite3.connect('database.db')
        c = conn.cursor()
        query = "INSERT INTO MEASUREMENTS (TIMESTMP,DIRECTION,BTS_ID,MR_ID,SIGNAL,QUALITY) VALUES (?, ?, ?, ?, ?, ?)"
        c.execute("PRAGMA synchronous = OFF")
        c.execute("PRAGMA journal_mode = OFF")
        conn.commit()
        conn.close
        payload = deque([], 1000000)
        while self._active or not self.thread_queue.empty():
            try:
                # number inside range is how many measurements will be sent in one request
                for n in range(self.batch_size):
                    payload.appendleft(self.thread_queue.get(block=True, timeout=0.05))
                c.executemany(query, payload)
                conn.commit()
                payload.clear()
            except Empty:
                c.executemany(query, payload)
                conn.commit()
                payload.clear()
            else:
                c.executemany(query, payload)
                conn.commit()
                payload.clear()
        conn.close()

    def stop(self):
        """
        Method for stopping the thread from working.
        Thread will be stopped only if self.thread_queue is empty
        """
        self._active = False


def check_for_handover(serving_sig_avg, signal, offset=OFFSET):
    """
    Checks if the conditions for performing handover met.

    :param signal: signal strenght of neighbour (Nx) cell
    :type signal: int
    :param serving_sig_avg: average signal strenght of serving (S0) cell
    :type serving_sig_avg: int or float
    :param offset: value of possible offset
    :type offset: int or float
    :rtype: bool
    """
    if serving_sig_avg == 0:
        return False
    return serving_sig_avg + offset < signal


def call_for_handover(bts_id, ms_id):
    """
    Formats string to required format for announcing possiblity of handover.

    :param bts_id: ID(Nx) of neighbour cell
    :type bts_id: str
    :param ms_id: ID of mobile radio
    :type ms_id: str
    """
    return "DL  {}  {}  HOBC".format(bts_id, ms_id)


def main():
    """
    Main logic and flow of PCAv2.
    """
    cfg = Config()
    cfg.setup()
    if not cfg.is_correct():
        return 1
    conf = cfg.settings
    reg = Register(window=cfg.settings['WINDOW'])
    thread_queue = Queue()
    if conf['DB'] == 'REMOTE':
        sender_worker = DBHTTPsender(thread_queue, thread_id=1)
    elif conf['DB'] == 'LOCAL':
        sender_worker = DBLocalsender(thread_queue, thread_id=1)
    # start of separate thread for http request post to insert to database
    sender_worker.start()
    for line in sys.stdin:
        raw_measurement = sanitize(line)
        if raw_measurement is None:
            continue
        measurement = MeasurementTuple(*raw_measurement)
        thread_queue.put(measurement)
        if measurement.bts_id != "S0":
            if measurement.direction == "DL" and measurement.signal != 0:
                if check_for_handover(reg.get_avg(measurement.direction, measurement.ms_id), measurement.signal):
                    print(call_for_handover(measurement.bts_id, measurement.ms_id))
                    continue
                else:
                    continue
            else:
                continue
        reg.insert(measurement.direction, measurement.ms_id, measurement.signal, measurement.quality, missing_limit=conf["MISSING_LIMIT"])
        if not reg.enough_measurements(measurement.direction, measurement.ms_id, min_measurements=conf["MIN_MEAS"]):
            print("{}  S0  {}  NCH".format(measurement.direction, measurement.ms_id))
            continue
        signal_correction = SignalCorrection(measurement.direction, measurement.ms_id)
        signal_correction.signal_avg = reg.get_avg(measurement.direction, measurement.ms_id, param='signals')
        signal_correction.quality_avg = reg.get_avg(measurement.direction, measurement.ms_id, param='qualities')
        signal_correction.get_signal_target_difference(target=conf["TARGET"], max_decr=conf["MAX_DECR"], max_incr=conf["MAX_INCR"])
        signal_correction.apply_hysteresis_filter(target=conf["TARGET"], hyst=conf["HYSTERESIS"])
        signal_correction.apply_quality_filter()
        print(signal_correction.get_output_format())
    sender_worker.stop()


if __name__ == '__main__':
    main()
