# -*- coding: utf-8 -*-

"""
Simple web server with one API endpoint to insert measurements processed by PCA, into database.

© 2017 by :ref:`Pull'n'Pusz <about>`
"""
import sqlite3

from bottle import request, route, run


@route('/insert', method='POST')
def db_insert():
    """
    Put measurements into database.
    """
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    measurements = request.json.values()
    query = "INSERT INTO MEASUREMENTS (TIMESTMP,DIRECTION,BTS_ID,MR_ID,SIGNAL,QUALITY) VALUES (?, ?, ?, ?, ?, ?)"
    c.execute("PRAGMA synchronous = OFF")
    c.execute("PRAGMA journal_mode = OFF")
    c.executemany(query, measurements)
    conn.commit()
    conn.close


if __name__ == '__main__':
    run(port=80, host='127.0.0.1')
