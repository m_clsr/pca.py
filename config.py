# -*- coding: utf-8 -*-

"""
Config module provide possibility for customize settings whitch are
crucial for PCA work. Config is created in/loaded from :ref:`config_pca.txt <config_pca.txt>`
in working directory.

© 2017 by :ref:`Pull'n'Pusz <about>`.
"""

import os


class Config(object):
    """
    Stores settings values and deals with config file.
    """

    default = {'TARGET': -75, 'MAX_INCR': 8, 'MAX_DECR': -4,
               'MISSING_LIMIT': 1, 'MIN_MEAS': 4, 'OFFSET': 3, 'HYSTERESIS': 3,
               'HOST': 'http://127.0.0.1:80/insert', 'WINDOW': 4, 'DB': 'LOCAL'}
    """dictionary storing default config valules"""

    def __init__(self):
        self.settings = self.default.copy()
        self.wrong_parameter = None

    def setup(self, route='config_pca.txt'):
        """
        Read and parse config

        :param route: relative path to config
        :type route: str
        """
        if os.path.isfile(route):
            with open(route, 'r') as cfg_file:
                for line in cfg_file:
                    if 'TARGET = ' in line:
                        val = int(line.split(' = ')[1])
                        if -45 >= val >= -95:
                            self.settings['TARGET'] = val
                        else:
                            self.wrong_parameter = 'TARGET'
                            break
                    elif 'MAX_INCR = ' in line:
                        val = int(line.split(' = ')[1])
                        if 16 >= val >= 1:
                            self.settings['MAX_INCR'] = val
                        else:
                            self.wrong_parameter = 'MAX_INCR'
                            break
                    elif 'MAX_DECR = ' in line:
                        val = int(line.split(' = ')[1])
                        if -1 >= val >= -16:
                            self.settings['MAX_DECR'] = val
                        else:
                            self.wrong_parameter = 'MAX_DECR'
                            break
                    elif 'MISSING_LIMIT = ' in line:
                        val = int(line.split(' = ')[1])
                        if 3 >= val >= 1:
                            self.settings['MISSING_LIMIT'] = val
                        else:
                            self.wrong_parameter = 'MISSING_LIMIT'
                            break
                    elif 'MIN_MEAS = ' in line:
                        val = int(line.split(' = ')[1])
                        if 8 >= val >= 1:
                            self.settings['MIN_MEAS'] = val
                        else:
                            self.wrong_parameter = 'MIN_MEAS'
                            break
                    elif 'OFFSET = ' in line:
                        val = int(line.split(' = ')[1])
                        if val >= 2:
                            self.settings['OFFSET'] = val
                        else:
                            self.wrong_parameter = 'OFFSET'
                            break
                    elif 'HYSTERESIS = ' in line:
                        val = int(line.split(' = ')[1])
                        if 10 >= val >= 0:
                            self.settings['HYSTERESIS'] = val
                        else:
                            self.wrong_parameter = 'HYSTERESIS'
                            break
                    elif 'HOST = ' in line:
                        val = line.split(' = ')[1]
                        self.settings['HOST'] = val
                    elif 'WINDOW = ' in line:
                        val = int(line.split(' = ')[1])
                        if 8 >= val >= 1:
                            self.settings['WINDOW'] = val
                        else:
                            self.wrong_parameter = 'WINDOW'
                            break
                    elif 'DB = ' in line:
                        val = line.split(' = ')[1]
                        if val == 'LOCAL':
                            self.settings['DB'] = val
                        elif val == 'REMOTE':
                            self.settings['DB'] = 'REMOTE'
                        else:
                            self.wrong_parameter = 'DB'
                            break
        else:
            def_cfg_text = """TARGET = {TARGET}\nMAX_INCR = {MAX_INCR}\n\
MAX_DECR = {MAX_DECR}\nMISSING_LIMIT = {MISSING_LIMIT}\n\
MIN_MEAS = {MIN_MEAS}\nOFFSET = {OFFSET}\nHOST = {HOST}\nHYSTERESIS = {HYSTERESIS}\
\nWINDOW = {WINDOW}\nDB = {DB}""".format(**self.default)
            with open('config_pca.txt', 'w') as cfg_file:
                cfg_file.write(def_cfg_text)

    def is_correct(self):
        """
        Checks if the loaded config is correct.

        :rtype: bool
        """
        if self.wrong_parameter is not None:
            print("Invalid value of {} in configuration file.\nPCA exection halted!".format(self.wrong_parameter))
            return False
        else:
            return True
