import pca
import pytest


def test_sanitize_if_signal_has_6element_DS():
    line = "UL S0 MS776 -75 1"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -75, 1]


def test_sanitize_if_signal_is_0_and_quality_is_5_for_missing():
    line = "UL S0 MS776 missing"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -9999, 5]


def test_sanitize_if_quality_is_not_there():
    line = "UL S0 MS776 -78"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -78, 5]


def test_sanitize_if_signal_is_not_there():
    line = "UL S0 MS776"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_direction():
    line = "Aaaban S0 MS776 -75 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_bts_id():
    line = "UL S5 MS776 -75 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_mr_id():
    line = "UL S0 M*&SG -75 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_signal_value():
    line = "UL S0 MS776 ah 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_signal_sign():
    line = "UL S0 MS776 78 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_quality_value():
    line = "UL S0 MS776 -75 9"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_wrong_quality_sign():
    line = "UL S0 MS776 -75 -1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_has_diff_no_of_spaces_between_values():
    line = "UL      S0      MS776    -75 1"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -75, 1]


def test_sanitize_if_signal_has_spaces_after_string():
    line = "UL         S0        MS776      -75    1    "
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -75, 1]


def test_sanitize_if_signal_has_diff_no_of_tabs_between_values():
    line = "UL" + "\t" + "S0" + "\t" + "MS776" + "\t" + "-75 " + "\t" + "1"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -75, 1]


def test_Register_enough_measurements_for_too_small_DS():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3]
    assert reg_test.enough_measurements("UL", "M7329", 4) is False


def test_Register_enough_measurements_for_4element_DS():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3, 4]
    assert reg_test.enough_measurements("UL", "M7329", 4) is True


def test_Register_enough_measurements_for_biggerthan4_DS():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3, 4, 5]
    assert reg_test.enough_measurements("UL", "M7329", 4) is True


def test_Register_enough_measurements_defcfg():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3, 4]
    assert reg_test.enough_measurements("UL", "M7329") is True


def test_Register_get_avg_if_correct_signal_values():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3, 4, 5, 6, 7, 8]
    assert reg_test.get_avg("UL", "M7329") == 1.968627450980392


def test_Register_get_avg_if_correct_signal_values_only_ones():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 1, 1, 1, 1, 1, 1, 1]
    assert reg_test.get_avg("UL", "M7329") == 1.000


def test_Register_get_avg_if_correct_signal_value_4numbers():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3, 4]
    assert reg_test.get_avg("UL", "M7329") == 1.7333333333333334


def test_Register_get_avg_if_too_many_values():
    reg_test = pca.Register()
    reg_test.struct["UL"]["M7329"]['signals'] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    with pytest.raises(KeyError):
        reg_test.get_avg("UL", "M7329")


def test_Register_get_avg_if_reg_is_empty():
    reg_test = pca.Register()
    assert reg_test.get_avg("UL", "Muuuuuu") == 0


def test_SignalCorrection_get_signal_target_diff_signal_min75db():
    signal_test = pca.SignalCorrection("UL", "M7329")
    signal_test.signal_avg = -75
    signal_test.get_signal_target_difference(target=-75, max_incr=8, max_decr=-4)
    assert signal_test.value == 0


def test_SignalCorrection_get_signal_target_diff_signal_max_incr():
    signal_test = pca.SignalCorrection("UL", "M7329")
    signal_test.signal_avg = -84
    signal_test.get_signal_target_difference(target=-75, max_incr=8, max_decr=-4)
    assert signal_test.value == 8


def test_SignalCorrection_get_signal_target_diff_signal_equal_max_incr():
    signal_test = pca.SignalCorrection("UL", "M7329")
    signal_test.signal_avg = -83
    signal_test.get_signal_target_difference(target=-75, max_incr=8, max_decr=-4)
    assert signal_test.value == 8


def test_SignalCorrection_get_signal_target_diff_signal_max_decr():
    signal_test = pca.SignalCorrection("UL", "M7329")
    signal_test.signal_avg = -69
    signal_test.get_signal_target_difference(target=-75, max_incr=8, max_decr=-4)
    assert signal_test.value == -4


def test_SignalCorrection_get_signal_target_diff_signal_equal_max_decr():
    signal_test = pca.SignalCorrection("UL", "M7329")
    signal_test.signal_avg = -71
    signal_test.get_signal_target_difference(target=-75, max_incr=8, max_decr=-4)
    assert signal_test.value == -4


def test_SignalCorrection_apply_hysteresis_filter_for_avg_75():
    hyst_test = pca.SignalCorrection("UL", "M7329")
    hyst_test.signal_avg = -75
    hyst_test.value = 0
    hyst_test.apply_hysteresis_filter(target=-75, hyst=1)
    assert hyst_test.value == 0


def test_SignalCorrection_apply_hysteresis_filter_for_avg_76():
    hyst_test = pca.SignalCorrection("UL", "M7329")
    hyst_test.signal_avg = -76
    hyst_test.value = -1
    hyst_test.apply_hysteresis_filter(target=-75, hyst=1)
    assert hyst_test.value == -1


def test_SignalCorrection_apply_hysteresis_filter_for_average_77():
    hyst_test = pca.SignalCorrection("UL", "M7329")
    hyst_test.signal_avg = -77
    hyst_test.value = 1
    hyst_test.apply_hysteresis_filter(target=-75, hyst=1)
    assert hyst_test.value == 1


def test_SignalCorrection_apply_hysteresis_filter_for_average_77_and_hist_3():
    hyst_test = pca.SignalCorrection("UL", "M7329")
    hyst_test.signal_avg = -77
    hyst_test.value = 1
    hyst_test.apply_hysteresis_filter(target=-75, hyst=3)
    assert hyst_test.value == 1


def test_SignalCorrection_get_output_format_for_minus_one():
    out_test = pca.SignalCorrection("UL", "M7329")
    out_test.value = -1
    assert out_test.get_output_format() == "UL  S0  M7329  DEC  1"


def test_SignalCorrection_get_output_format_for_zero():
    out_test = pca.SignalCorrection("UL", "M7329")
    out_test.value = 0
    assert out_test.get_output_format() == "UL  S0  M7329  NCH"


def test_SignalCorrection_get_output_format_for_plus_one():
    out_test = pca.SignalCorrection("UL", "M7329")
    out_test.value = 1
    assert out_test.get_output_format() == "UL  S0  M7329  INC  1"


def test_SignalCorrection_apply_quality_filter_lessthantwo():
    quality_test = pca.SignalCorrection("UL", "M7329")
    quality_test.quality_avg = 1
    quality_test.value = 10
    quality_test.apply_quality_filter()
    assert quality_test.value == 10


def test_SignalCorrection_apply_quality_filter_q_morethanfour_v_morethantwo():
    quality_test = pca.SignalCorrection("UL", "M7329")
    quality_test.quality_avg = 5
    quality_test.value = 3
    quality_test.apply_quality_filter()
    assert quality_test.value == 3


def test_SignalCorrection_apply_quality_filter_q_morethanfour_v_lessthantwo():
    quality_test = pca.SignalCorrection("UL", "M7329")
    quality_test.quality_avg = 5
    quality_test.value = 1
    quality_test.apply_quality_filter()
    assert quality_test.value == 2


def test_SignalCorrection_apply_quality_filter_q_lessthanfour_v_morethanzero():
    quality_test = pca.SignalCorrection("UL", "M7329")
    quality_test.quality_avg = 3
    quality_test.value = 5
    quality_test.apply_quality_filter()
    assert quality_test.value == 5


def test_SignalCorrection_apply_quality_filter_q_lessthanfour_v_lessthanzero():
    quality_test = pca.SignalCorrection("UL", "M7329")
    quality_test.quality_avg = 2
    quality_test.value = -3
    quality_test.apply_quality_filter()
    assert quality_test.value == 0


def test_Register_insert_signal():
    ins_test = pca.Register()
    ins_test.insert("UL", "Masdfx", -75, 0)
    assert ins_test.struct["UL"]["Masdfx"]["signals"][0] == -75


def test_Register_insert_quality():
    ins_test = pca.Register()
    ins_test.insert("UL", "Masdfx", -75, 0)
    assert ins_test.struct["UL"]["Masdfx"]["qualities"][0] == 0


def test_Register_missing_is_zero_at_first_nonmissing_insert():
    ins_test = pca.Register()
    ins_test.insert("UL", "Masdfx", -75, 0)
    assert ins_test.struct["UL"]["Masdfx"]["missing"] == 0


def test_Register_insert_missing_incrementing():
    ins_test = pca.Register()
    ins_test.insert("UL", "M73291", -75, -9999, missing_limit=3)
    ins_test.insert("UL", "M73291", -9999, 5, missing_limit=3)
    ins_test.insert("UL", "M73291", -9999, 5, missing_limit=3)
    ins_test.insert("UL", "M73291", -9999, 5, missing_limit=3)
    assert ins_test.struct["UL"]["M73291"]["missing"] == 3


def test_Register_insert_missing_increment_reset():
    ins_test = pca.Register()
    ins_test.insert("UL", "M73291", -75, 0, missing_limit=3)
    ins_test.insert("UL", "M73291", 0, 5, missing_limit=3)
    ins_test.insert("UL", "M73291", 0, 5, missing_limit=3)
    ins_test.insert("UL", "M73291", 0, 5, missing_limit=3)
    ins_test.insert("UL", "M73291", -66, 5, missing_limit=3)
    assert ins_test.struct["UL"]["M73291"]["missing"] == 0


def test_Register_insert_signal_is_missing_first_appearance_of_mr_dir_bts():
    ins_test = pca.Register()
    ins_test.insert("UL", "M666", -9999, 5)
    assert ins_test.struct["UL"]["M666"]["signals"][0] == -95


def test_Register_insert_missing_signal_below_missing_threshold():
    ins_test = pca.Register()
    ins_test.insert("UL", "M11", -75, 5)
    ins_test.insert("UL", "M11", -9999, 5, missing_limit=4)
    assert ins_test.struct["UL"]["M11"]["signals"][0] == ins_test.struct["UL"]["M11"]["signals"][1]


def test_call_for_handover():
    assert pca.call_for_handover("N6", "MSWTFXBBQ") == "DL  N6  MSWTFXBBQ  HOBC"


def test_check_for_handover_S0_average_signal_zero():
    assert pca.check_for_handover(0, -75, offset=3) is False


def test_check_for_handover_S0_weaker_sig_than_Nx():
    assert pca.check_for_handover(-75, -45, offset=3) is True


def test_check_for_handover_S0_stronger_sig_than_Nx():
    assert pca.check_for_handover(-60, -75, offset=3) is False


def test_check_for_handover_S0_weaker_sig_than_Nx_but_in_offset():
    assert pca.check_for_handover(-75, -72, offset=5) is False


def test_sanitize_if_signal_higher_than_range():
    line = "UL S0 MS776 -44 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_lower_than_range():
    line = "UL S0 MS776 -96 1"
    assert pca.sanitize(line) is None


def test_sanitize_if_signal_on_range_higher_edge():
    line = "UL S0 MS776 -45 1"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -45, 1]


def test_sanitize_if_signal_on_range_lower_edge():
    line = "UL S0 MS776 -95 1"
    assert pca.sanitize(line)[1:6] == ["UL", "S0", "MS776", -95, 1]


def test_DBHTTPsender_stop_method():
    dbs = pca.DBHTTPsender(None, 1)
    dbs.stop()
    assert dbs._active is False
