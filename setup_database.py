import sqlite3
import os

if not os.path.isfile('database.db'):
    conn = sqlite3.connect('database.db')
    conn.execute('''CREATE TABLE MEASUREMENTS
             (ID INTEGER PRIMARY KEY AUTOINCREMENT,
             TIMESTMP REAL NOT NULL,
             DIRECTION TEXT NOT NULL,
             BTS_ID TEXT NOT NULL,
             MR_ID TEXT NOT NULL,
             SIGNAL REAL NOT NULL,
             QUALITY INTEGER NOT NULL);''')
    conn.commit()
    conn.close()
    print("Database has been set up succesfully")
