.. _algo:

PCAv2 core algorythm and workflow
=================================

General idea
------------

The PCAv2 algorithm tries to increase or decrease MS or BTS power to match target for signal
strength. Hysteresis value is used to define range of signals when correction is made only by value of -1dBm or none at all. If measured
value is differrent than target, instruction to decrease or increaser power is sent.

- If measured value is below target, instruction to increase power is sent.

- If measured value is above target, instruction to decrease power is sent.

- If measured value falls into expected range no action is required:

              target-1dB <= signal strength <= target+1dB

Signal strength calculations
----------------------------

The value of measurement matched with target is calculated of 1 to 8 last measurement reports.
The PCAv2 does not send command when number of measurements collected is less than 4. (beginning
of communication). It may happen that some measurements are missing – they should arrive every
500ms, if not, the system interpolates:

- first missing measurements – value of previous one

- number of allowed missing measurement recovered as previous value is configurable from 1 to 3

- next CONSECUTIVE missing measurements are replaced with value -95dBm

- weight of each next measurement (first in calculations is last received) is dropping, if last measurement has weight 1, than one before last 1⁄2 , next 1⁄4, etc.

- when received (average) signal is within hysteresis difference from target, algorithm does not stop, but maximum step is 1 dB, algorithm send NCH command when measurement to target is less than 1dB

Quality of signal
-----------------

Quality, from 0 to 5, indicates the level of errors (Bit Error Rate), and this
measurement may override decisions made on power measurements only.

Procedure: Quality is processed similar way as power MS:

 * average is calculated over size of window with weights.
 
 * all missing Q MRs are considered = 5.

#) if Q is less than 2, algorithm works normally

#) if Q is in range of <2;4), algorithm is not allowed to decrease power. If that would be decision from power MS no change is used instead, increase is executed normally.

#) if Q is 4 or higher, decision is always to increase power, by minimum 2 dB or bigger if suchcalculated from power MS

