.. PCA documentation master file, created by
   sphinx-quickstart on Wed Jul 26 23:26:53 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Power Control Algorithm v2 (PCA.py) documentation!
=============================================================

The presented Power Control Algorithm is dedicated to adjust the transmitting power of Base Transceiver Station (BTS) and Mobile Station (MS) if it is needed. Algorithm is based on measurement reports of BTS and MS. The purpose of PCAv2 is to save power of BTS and MS, reduce the interference of the network and increase the quality of network.

pca.py :
   #) reads *sys.stdin*
   #) asses if captured input is correct
   #) stores measurement in SQL database locally or remotely
   #) if its desired by operator, checks for MS handover opportunity 
   #) evaluates signal correction request for BTS S0 - MS pair
   #) writes response to *sys.stdout*

The latest version is PCA.py 1.0.0, released July 25th 2017. It is supported on:
   * Ubuntu 16.04 LTS & Python 3.5.2

Quick start
-----------

#) Clone git repository:

    .. code-block:: console   
        
        git clone https://bitbucket.org/m_clsr/pca.py.git
        cd pca

   or

    .. code-block:: console   
        
        wget https://bitbucket.org/m_clsr/pca.py/get/f970a3fcea34.zip && unzip f970a3fcea34.zip
	mv m_clsr-pca.py-f970a3fcea34/ pca
        cd pca

#) Install dependencies:
    
    .. code-block:: console

        pip3 install -r requirements-pca.txt

#) Setup sqlite database localy in current directory:
    
    .. code-block:: console

        python3 database_setup.py

#) Start feeding input of pca.py with measurements:
    
    .. code-block:: console

       your_process_producing_measurements | python3 pca.py

Using PCA.py
------------

PCA.py is meant to be used as script, reading input from stdin and providing output to stdout.
Workflow and working parameters can be tuned by editing conf_pca.txt. If there is no such file,
it will be created with default configuration parameters at first execution of pca.py.

Getting help
------------

Feel free to contact us:

   * `Michał Cieślar`_
   * `Katarzyna Grzywacz`_
   * `Daria Radoszewska`_

.. _Michał Cieślar: michal.cieslar@ust-global.com
.. _Katarzyna Grzywacz: katarzyna.grzywacz@ust-global.com
.. _Daria Radoszewska: daria.radoszewska@ust-global.com

Bug reports are gladly accepted at the `Bitbucket issue tracker`_.
Bitbucket also hosts the `code repository`_.

.. _Bitbucket issue tracker: https://bitbucket.org/m_clsr/pca.py/issues
.. _code repository: https://bitbucket.org/m_clsr/pca.py/

The User Guide
--------------

.. toctree::
   :maxdepth: 2 
   :caption: User guide:

   intro
   instalation
   usage

The Developer Guide
-------------------

.. toctree::
   :maxdepth: 1   
   :caption: Developer Guide:

   codstyle
   algo
   pca
   config
   web_app

Navigate
--------

* :ref:`genindex`
* :ref:`search`
* :ref:`about`
