.. _usage:

Usage
=====

.. _`db_setup`:

Set up database
---------------

PCA requires sql database for storing measurement, either in pca.py working directory(:ref:`DB = LOCAL <conf_DB>`) or in web_app.py working directory(:ref:`DB = REMOTE <conf_DB>`).

For establishing database file and its scheme:

    .. code-block:: console   

        cd pca
        python3 setup_database.py

After sucessfull databse setup, there should appear in console:

    .. code-block:: console

        Database has been set up succesfully

And database.db file should be present.

Vaild input format
------------------

Input line should contain a combination of informations in four columns separated with
any number of white characters. Fifth column is optional.

.. table:: Input format
   :widths: auto
   :align: center

   =========================  =============  ==========================================  ============================  ==============
   direction of transmission  cell identity  MS identity                                 signal strength in dBm        signal quality
   =========================  =============  ==========================================  ============================  ==============
   "DL" or "UL"               {S0, N1 - N6}  Any combination of alphanumeric characters  integer <-95;-45> or missing  {0,1,2,3,4,5}
   =========================  =============  ==========================================  ============================  ==============

Direction of transmission:

  * DL ​ (downlink) - Signal is sent from BTS to MS

  * UP ​ (uplink) - Signal is sent from MS to BTS

Cell identity:

  * S0 ​- serving cell,

  * N[1-6] - neighbour cell

Examples of valid input:

 .. code-block:: console

   DL  S0  MS111  -45  1
   UL  S0  MS222  missing
   DL  N6  mswtfx -75
   UL N1 1232abc -55 5 

Running pca.py
--------------

Start your measurements producing process and redirect its *stdout* to pca.py *stdin*: 
    
    .. code-block:: console

        your_process_producing_measurements | python3 pca.py

.. _`config_pca.txt`:

Understanding config_pca.txt
----------------------------

Parameters which can be changed by conf_pca.txt:

.. _`conf_TARGET`:

TARGET - Desired value of signal strength.

* *Correct values: <-95;-45>*

.. _`conf_MAX_INCR`:

MAX_INCR - Maximum possible increase of signal strength.

* *Correct values: <1;16>*

.. _`conf_MAX_DECR`:

MAX_DECR - Maximum possible decrease of signal strength.

* *Correct values: <-16;-1>*

.. _`conf_MISSING_LIMIT`:

MISSING_LIMIT - Number of signal strength measurement admissible to be missing, before assigning value of -95dBm.

* *Correct values: {1, 2, 3}*

.. _`conf_MIN_MEAS`:

MIN_MEAS - Minimal number of measurements required, before PCA will start genereating signal stregth change outputs.

* *Correct values: {1, 2, 3, 4, 5, 6, 7, 8}*

.. _`conf_OFFSET`:

OFFSET - Offset from signal strength target, for assesing handover viability.

* *Correct values: <1;+inf>*

.. _`conf_HYSTERESIS`:

HYSTERESIS -  Hysteresis for target signal strength.

* *Correct values: <0;10>*

.. _`conf_WINDOW`:

WINDOW - How many measurements will be taken into consideration, while calculating average signal quality and average signal strength.

* *Correct values: {1, 2, 3, 4, 5, 6, 7, 8}*

.. _`conf_HOST`:

HOST - Address and port of host providing web server with database access. This parameter only matters when 'DB = REMOTE'.

.. _`conf_DB`:

DB - Location of database for storing measurements registered.

* *Correct values: {'REMOTE', 'LOCAL'}*
