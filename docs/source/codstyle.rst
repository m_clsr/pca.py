.. _codstyle:

Coding style 
============

Google Python Style Guide
-------------------------

PCA.py project follows `Python Style Guide established by Google <https://google.github.io/styleguide/pyguide.html>`_ based of `PEP8 <https://www.python.org/dev/peps/pep-0008/>`_.

**EXCEPT**
----------

We don't enforce any strict maximum number of characters per single line. Use common sense and matter readablity of code/idea at first place.
