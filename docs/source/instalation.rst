.. _instalation:

Installation
============

Get the source code
-------------------

Easiest way to acces latest version of PCA.py is to get copy of repository on your machnine. You can do that by simply cloning git repo or downloading it.

* Clone git repository:

    .. code-block:: console   

        git clone https://bitbucket.org/m_clsr/pca.py.git
        cd pca

Or
  
* wget and uzip:
    .. code-block:: console   

        wget https://bitbucket.org/m_clsr/pca.py/get/f970a3fcea34.zip && unzip f970a3fcea34.zip
	mv m_clsr-pca.py-f970a3fcea34 pca
        cd pca

Or

* Go to bitbucket repo page, and download it, and uzip to your liking:

    .. code-block:: console   

        https://bitbucket.org/m_clsr/pca.py/downloads

Install dependencies
--------------------

PCA.py is based off the goods from rich Python standard library. However few external packages are required.

* If you have `PyPI package manager <https://pip.pypa.io/en/stable/>`_:
    
    .. code-block:: console   

        cd pca
        pip3 install -r requirements-pca.txt

  Webserver requirements:  

    .. code-block:: console

        cd pca
        pip3 install -r webserv/requirements-web_app.txt

* If you don't have pip installed, this `Python installation guide <http://docs.python-guide.org/en/latest/starting/install3/linux/#install3-linux/>`_ can guide you through the process.




