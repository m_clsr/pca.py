.. _introduction:

Introduction
============

Disclamer
---------

This project was created as graded group assignment during Akamai Technical Academy 4.0. Usage is on your own risk!

.. _`apache2`:

Apache2 License
---------------

PCA.py is released under terms of `Apache2 License`_.

.. _`GPL Licensed`: http://www.opensource.org/licenses/gpl-license.php
.. _`Apache2 License`: http://opensource.org/licenses/Apache-2.0


PCA.py License
----------------

    .. include:: ../../LICENSE

